'''
Description: 实体
Author: liyong
Date: 2021-05-27 11:42:38
LastEditTime: 2021-06-07 11:46:56
LastEditors: liyong
'''
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import AbstractUser
import django.utils.timezone as timezone
from api.enums import AdminStatus, RoleStatus
# Create your models here.

# 管理员


class Admin(AbstractUser):
    Id = models.AutoField(primary_key=True, db_column='Id',
                          verbose_name='编号', help_text='编号')
    Account = models.CharField(
        max_length=20, db_column='Account', verbose_name='账号', help_text='账号')
    Password = models.CharField(
        max_length=50, db_column='Password',  verbose_name='密码', help_text='密码')
    Name = models.CharField(max_length=20,
                            db_column='Name', verbose_name='姓名', help_text='姓名')
    Gender = models.IntegerField(
        db_column='Gender', default=0, verbose_name='性别', help_text='性别')
    Avatar = models.CharField(
        max_length=200, null=True, db_column='Avatar', verbose_name='头像', help_text='头像')
    EmployeeId = models.IntegerField(
        db_column='EmployeeId', default=0, verbose_name='员工编号', help_text='员工编号')
    RoleId = models.IntegerField(
        db_column='RoleId', default=0, verbose_name='角色编号', help_text='角色编号')
    Creator = models.IntegerField(
        db_column='Creator', default=0, verbose_name='创建人编号', help_text='创建人编号')
    CreateTime = models.DateTimeField(
        db_column='CreateTime', default=timezone.now, verbose_name='创建时间', help_text='创建时间')
    LastModifier = models.IntegerField(
        db_column='LastModifier', default=0, verbose_name='最后修改人编号', help_text='最后修改人编号')
    LastModifyTime = models.DateTimeField(
        db_column='LastModifyTime', default=timezone.now, verbose_name='最后修改时间', help_text='最后修改时间')
    LastLoginIP = models.CharField(
        max_length=20, null=True, db_column='LastLoginIP', verbose_name='最后登录IP', help_text='最后登录IP')
    LastLoginTime = models.DateTimeField(
        null=True, db_column='LastLoginTime', verbose_name='最后登录时间', help_text='最后登录时间')
    IsRoot = models.BooleanField(
        db_column='IsRoot', default=0, verbose_name='是否内置账号', help_text='是否内置账号')
    Status = models.IntegerField(
        db_column='Status', default=1, verbose_name='状态', help_text='状态')
    Remark = models.CharField(max_length=500,
                              db_column='Remark', verbose_name='备注', help_text='备注', null=True)
    IsDeleted = models.BooleanField(
        db_column='IsDeleted', default=False, verbose_name='是否逻辑删除', help_text='是否逻辑删除')

    class Meta:
        app_label = '管理员'
        db_table = 'admin'
        verbose_name = '管理员'

# 角色


class Role(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id',
                          verbose_name='编号', help_text='编号')
    Name = models.CharField(max_length=20,
                            db_column='Name', verbose_name='名称', help_text='名称')
    Creator = models.IntegerField(
        db_column='Creator', default=0, verbose_name='创建人编号', help_text='创建人编号')
    CreateTime = models.DateTimeField(
        db_column='CreateTime', default=timezone.now, verbose_name='创建时间', help_text='创建时间')
    LastModifier = models.IntegerField(
        db_column='LastModifier', default=0, verbose_name='最后修改人编号', help_text='最后修改人编号')
    LastModifyTime = models.DateTimeField(
        db_column='LastModifyTime', default=timezone.now, verbose_name='最后修改时间', help_text='最后修改时间')
    Status = models.IntegerField(
        db_column='Status', default=1, verbose_name='状态', help_text='状态')
    Remark = models.CharField(max_length=500,
                              db_column='Remark', verbose_name='备注', help_text='备注', null=True)
    IsDeleted = models.BooleanField(
        db_column='IsDeleted', default=False, verbose_name='是否逻辑删除', help_text='是否逻辑删除')

    class Meta:
        app_label = '角色'
        db_table = 'role'
        verbose_name = '角色'

# 员工


class Admin(models.Model):
    Id = models.AutoField(primary_key=True, db_column='Id',
                          verbose_name='编号', help_text='编号')
    Name = models.CharField(max_length=20,
                            db_column='Name', verbose_name='姓名', help_text='姓名')
    Gender = models.IntegerField(
        db_column='Gender', default=0, verbose_name='性别', help_text='性别')
    Avatar = models.CharField(
        max_length=200, null=True, db_column='Avatar', verbose_name='头像', help_text='头像')
    Creator = models.IntegerField(
        db_column='Creator', default=0, verbose_name='创建人编号', help_text='创建人编号')
    CreateTime = models.DateTimeField(
        db_column='CreateTime', default=timezone.now, verbose_name='创建时间', help_text='创建时间')
    LastModifier = models.IntegerField(
        db_column='LastModifier', default=0, verbose_name='最后修改人编号', help_text='最后修改人编号')
    LastModifyTime = models.DateTimeField(
        db_column='LastModifyTime', default=timezone.now, verbose_name='最后修改时间', help_text='最后修改时间')
    Status = models.IntegerField(
        db_column='Status', default=1, verbose_name='状态', help_text='状态')
    Remark = models.CharField(max_length=500,
                              db_column='Remark', verbose_name='备注', help_text='备注', null=True)
    IsDeleted = models.BooleanField(
        db_column='IsDeleted', default=False, verbose_name='是否逻辑删除', help_text='是否逻辑删除')

    class Meta:
        app_label = '员工'
        db_table = 'employee'
        verbose_name = '员工'
