'''
Description: 接口
Author: liyong
Date: 2021-05-27 11:42:38
LastEditTime: 2021-06-07 10:13:53
LastEditors: liyong
'''
from __future__ import unicode_literals
from django.shortcuts import render

# Create your views here.
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import serializers
from api.models import Admin
from api.serializers import AdminSerializer
from django.core.paginator import Paginator  # 分页
from rest_framework.parsers import JSONParser
from rest_framework_jwt.settings import api_settings


@csrf_exempt
# 管理员
def admins(request):

    # 获取分页
    if request.method == 'GET':
        account = request.GET['account']
        if account is not None:
            list = Admin.objects.filter()
        else:
            list = Admin.objects.all()
        pageList = Paginator(list, request.GET['pageSize']).page(
            request.GET['pageIndex'])
        return JsonResponse({"code": 200, "data": AdminSerializer(pageList, many=True).data, "msg": "success"}, json_dumps_params={"ensure_ascii": False})
    # 创建
    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = AdminSerializer(data=data)
        if serializer.is_valid:
            serializer.save()
            return JsonResponse({"code": 200, "data": serializer.data, "msg": "success"})
    # 更新
    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = AdminSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse({"code": 200, "data": serializer.data, "msg": "success"})
        return JsonResponse({'code': 500, 'data': serializer.errors, 'msg': serializer.error_messages})
    # 删除
    elif request.method == 'DELETE':
        Admin.objects.filter(id=request.GET['id']).update(
            **{'IsDeleted': True})
        return JsonResponse({"code": 200, "data": '', "msg": "success"})

# 登录


def login(request):
    if request.method == 'POST':
        account = request.GET['account']
        password = request.GET['password']
        if account and password is not None:
            entity = Admin.objects.filter(
                account=account, password=password).distinct()
            jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
            jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
            payload = jwt_payload_handler(entity)
            token = jwt_encode_handler(payload)
            return JsonResponse({"code": 200, "data": {"data": entity, "token": token}, "msg": "success"})
        else:
            return JsonResponse({"code": 500, "data": "", "msg": "参数非法！"})
