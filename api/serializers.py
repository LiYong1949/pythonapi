'''
Description: 序列化
Author: liyong
Date: 2021-05-27 17:13:45
LastEditTime: 2021-05-31 14:29:15
LastEditors: liyong
'''
from rest_framework import serializers
from api.models import Admin


class AdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = Admin
        fields = '__all__'

    def create(self, validated_data):
        return Admin.objects.create(validated_data)

    def update(self, instance, validated_data):
        return Admin.objects.update(instance, validated_data)
