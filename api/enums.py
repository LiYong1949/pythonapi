'''
Description: 
Author: liyong
Date: 2021-05-28 15:49:17
LastEditTime: 2021-05-28 15:58:34
LastEditors: liyong
'''


from enum import IntEnum

# 管理员状态


class AdminStatus(IntEnum):
    # 正常
    Normal = 1,
    # 锁定
    Lock = 2

# 角色状态


class RoleStatus(IntEnum):
    # 正常
    Normal = 1,
    # 锁定
    Lock = 2
