'''
Description: 
Author: liyong
Date: 2021-05-27 11:42:38
LastEditTime: 2021-05-27 15:01:53
LastEditors: liyong
'''
from __future__ import unicode_literals
from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = 'api'
